package com.example.epractical

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.epractical.databinding.ActivityMainBinding
import java.util.*
import javax.sql.DataSource


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.clickToFetch.setOnClickListener {
            loadImage()
        }
        updateView(Utils.isNetConnected(this))
        Utils.loadSavedImage(this, binding.loadImage)
        setConnectivityListner()
        setCurrentLanguage(applicationContext)

        binding.changeLanguage.setOnClickListener {
            val context = if (binding.changeLanguage.text.equals(getString(R.string.en))) {
                LocaleHelper.setLocale(this, "hi");
            } else {
                LocaleHelper.setLocale(this, "en");
            }
            setCurrentLanguage(context)
        }
    }

    private fun updateView(status: Boolean) {
        if (status) {
            binding.noDataFound.visibility = View.GONE
            binding.clickToFetch.visibility = View.VISIBLE
        } else {
            binding.noDataFound.visibility = View.VISIBLE
            binding.clickToFetch.visibility = View.GONE
        }
    }

    private fun loadImage() {
        if (Utils.isNetConnected(this)) {
            val image =
                "https://picsum.photos/id/" + Utils.getRandomNumber() + "/" + Utils.getRandomHeightAndWidth() + "/" + Utils.getRandomHeightAndWidth() + ""

            binding.loadingImageText.visibility = View.VISIBLE
            Glide.with(this)
                .load(image)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.loadingImageText.text = applicationContext.resources.getString(R.string.something_went_wrong)
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: com.bumptech.glide.load.DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.loadingImageText.visibility = View.GONE
                        return false
                    }
                })
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.loadImage)
            Utils.saveImage(this, image)
        } else {
            updateView(false)
        }
    }

    private fun setConnectivityListner() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.let {
            it.registerDefaultNetworkCallback(@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    runOnUiThread {
                        updateView(true)
                    }
                }

                override fun onLost(network: Network) {
                    runOnUiThread {
                        updateView(false)
                    }
                }
            })
        }
    }

    private fun setCurrentLanguage(context: Context) {
        //               messageView.setText(context.resources.getString(R.string.language));
        val current: Locale = context.resources.configuration.locale
        if (current.displayLanguage.lowercase().contains("en")) {
            binding.changeLanguage.text = context.resources.getString(R.string.en)
        } else {
            binding.changeLanguage.text = context.resources.getString(R.string.hi)
        }
        binding.clickToFetch.text = context.resources.getString(R.string.click_to_get_new_image)
        binding.titleText.text = context.resources.getString(R.string.welcome_to_e_practical)
        binding.internetText.text = context.resources.getString(R.string.check_internet_connection)
        binding.loadingImageText.text =
            context.resources.getString(R.string.please_wait_image_is_loading)
    }
}
