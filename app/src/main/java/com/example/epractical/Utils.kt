package com.example.epractical


import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.os.Build
import android.preference.PreferenceManager
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.util.*


object Utils {
    fun isNetConnected(mContext: Context): Boolean {
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

    fun getRandomNumber(): Int {
        return Random().nextInt(500 - 1 + 1) + 1
    }

    fun getRandomHeightAndWidth(): Int {
        return Random().nextInt(700 - 300 + 1) + 300
    }

    fun loadSavedImage(context: Context, imageview: ImageView) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        Glide.with(context)
            .load(preferences.getString("LastImage", ""))
            .error(R.drawable.image_placeholder)
            .placeholder(R.drawable.image_placeholder)
            .into(imageview)
    }

    fun saveImage(context: Context, image: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString("LastImage", image)
        editor.apply()
    }

}

